'use strict'

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser'); 
const api = require('./api/routes');
const morgan = require('morgan');
require('./config');

const app = express();

app.use(validator());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan(process.env.NODE_ENV));
app.use('/', api);


module.exports=app;