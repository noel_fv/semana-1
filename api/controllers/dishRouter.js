'use strict'

const _ = require('underscore');



function get(req, res) {
  res.send("Hello world!");
};

function save(req, res) {
  res.send("agregando un dish " + req.body.name);
};

function update(req, res,next) {
  let dishId = req.params.dishId;

  if(dishId=== undefined){
    res.statusCode=403;
    res.send("Operacion PUT no soportada en dishe");
  }else{
    res.json({
      ok: true,
      dishId
    });
  }

};

function remove(req, res) {
  let dishId = req.params.dishId;

  if(dishId=== undefined){
    res.send("delete all dishes");
  }else{
    res.json({
      ok: true,
      message: "Delete id " +dishId
    });
  }
};


function findByID(req, res) {
    let dishId = req.params.dishId;
    res.json({
      ok: true,
      dishId
    });

  };

  module.exports = {
    findByID,
    get,
    update,
    save,
    remove
};