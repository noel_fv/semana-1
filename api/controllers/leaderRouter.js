'use strict'

const _ = require('underscore');

function get(req, res) {
  res.send("Hello leader");
};

function findByID(req, res) {
    let leaderId = req.params.leaderId;
    res.json({
      ok: true,
      leaderId
    });

  };

  function update(req, res) {
    let leaderId = req.params.leaderId;
  
    if(leaderId=== undefined){
      res.statusCode=403;
      res.send("Operacion PUT no soportada en leader");
    }else{
      res.json({
        ok: true,
        leaderId
      });
    }
  
  };
  
  function remove(req, res) {
    let leaderId = req.params.leaderId;
  
    if(leaderId=== undefined){
      res.send("delete all leaders");
    }else{
      res.json({
        ok: true,
        message: "Delete id " +leaderId
      });
    }
  };

  function save(req, res) {
    res.send("agregando un leader " + req.body.name);
  };

  module.exports = {
    get,
    findByID,
    update,
    remove,
    save
};